import Vue from 'nativescript-vue'
import App from './components/App'
import VueDevtools from 'nativescript-vue-devtools'
import { TNSFancyAlert, TNSFancyAlertButton } from "nativescript-fancyalert";

com.uxcam.UXCam.startWithKey("gtf23cqop5u44nv");
console.log(com.uxcam.UXCam)
com.uxcam.UXCam.setAutomaticScreenNameTagging(false);


if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
// import store from './store'

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

// Registering Card View
Vue.registerElement(
  'CardView',
  () => require('@nstudio/nativescript-cardview').CardView
);

Vue.registerElement(
	"Gradient", 
	() => require("nativescript-gradient").Gradient
);

Vue.registerElement('RadSideDrawer', 
	() => require('nativescript-ui-sidedrawer').RadSideDrawer
);


new Vue({
  render: h => h('frame', [h(App)])
}).$start()
